# Release Notes
2024-09-20

## Added

## Changed

* Upgrade Asterisk 20.9.3.
* Customer id module, send DTMF to fastAGI and them set channels vars into SIP Headers in order to send to agent tool.

## Fixed

* http ARI socket IPADDR for HA scenary.
* Asterisk conf and sounds container volumes when the omnileads UID OS is diferent to 1000.

## Removed
